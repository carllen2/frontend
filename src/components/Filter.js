import "../css/Filter.css";
import BookCoverUnavailable from '../images/BookCoverUnavailable.png';

const Filter = ({lstOfRelatedBooks, setLstOfRelatedBooks, getSearchStatus}) => {

    const structFilterType = {
        title: "title",
        publishedDate: "publish_date"
    }

    const SortBooks = (key) => {
        const sortedRelatedBooks = [...lstOfRelatedBooks].sort((first, second) => second[key] === first[key] ? 0 : second[key] > first[key] ? -1 : 1 );
        setLstOfRelatedBooks(sortedRelatedBooks);
        getSearchStatus(true);
    }

    const GetBooks = (title) => {  
    fetch(`http://openlibrary.org/search.json?q=${title.replace(" ", "+")}`)
      .then(response => response.json())
      .then(data => {
        // handle success
        const lstOfBooks = [];
        let docs = data["docs"]; 

        for (const doc of docs) {
        const filterPubDate = new RegExp(String(doc["first_publish_year"]));
        const publishDate = "publish_date" in doc ? doc["publish_date"].filter(date => filterPubDate.test(date))[0] : "Uknown Published Date";
        lstOfBooks.push(
            {
                "title": doc["title"],
                "book_cover": doc["cover_edition_key"] !== undefined ? `http://covers.openlibrary.org/b/olid/${doc["cover_edition_key"]}-M.jpg` : BookCoverUnavailable,
                "author": doc["author_name"] !== undefined ? doc["author_name"] : "Unkown Author",
                "publish_date": publishDate
            }
        );
        }

        setLstOfRelatedBooks(lstOfBooks);
        getSearchStatus(true);
      })    
    .catch(err => {
        // handle error
        getSearchStatus(false);
        console.error(err);
    })
    }

    return (
    <div id="filterContainer">
        <input type="text" id="search" aria-label="search" placeholder="Search"/>
        <button data-testid="search-button" onClick = {() => {
            GetBooks(document.getElementById("search").value);
        }}>Search</button>
        <input data-testid="filter-title-radio" type="radio" name="filter" id="filterTitle" onClick={() => {
            SortBooks(structFilterType.title);
        }}/>
        <label data-testid="filter-title-label-m" htmlFor="filterTitle" className="lbl-m">Sort by Title</label>
        <label data-testid="filter-title-label-s" htmlFor="filterTitle" className="lbl-s">Title</label>
        <input data-testid="filter-publish-date-radio" type="radio" name="filter" id="filterPublishedDate" onClick={() => {
            SortBooks(structFilterType.publishedDate);
        }}/>
        <label data-testid="filter-publish-date-label-m" htmlFor="filterPublishedDate" className="lbl-m">Sort by Published Date</label>
        <label data-testid="filter-publish-date-label-s" htmlFor="filterPublishedDate" className="lbl-s">Date</label>
    </div>
    );
}

export default Filter;