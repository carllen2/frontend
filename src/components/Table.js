import "../css/Table.css";

const Table = (props) => {

    return (
      <div id="tableContainer">
        <table data-testid="table">
            <tbody>
              {
                props.lstOfRelatedBooks.map(book => 
                    <tr>
                        <td >
                          <img src={book.book_cover} alt={book.title}/></td>
                        <td>
                          <span className="title">{book.title}</span><br/>
                          {book.author}<br/>
                          {book.publish_date}
                        </td>
                    </tr>
                )
              }
            </tbody>         
        </table>
      </div>

    );
  }
  
export default Table;