import {useState} from 'react';
import Filter from './Filter';
import Table from './Table';

const App = () => {

    const [arebooksRetrieved, getSearchStatus] = useState(false);
    const [lstOfRelatedBooks, setLstOfRelatedBooks] = useState([]);
  
    return (
      <div>
        <Filter lstOfRelatedBooks = {lstOfRelatedBooks} setLstOfRelatedBooks = {setLstOfRelatedBooks} getSearchStatus = {getSearchStatus}/>
        {arebooksRetrieved && <Table lstOfRelatedBooks = {lstOfRelatedBooks}/>}
      </div>
    );
}

export default App;