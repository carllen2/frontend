import React from "react";
import { render } from "@testing-library/react";
import Filter from "../components/Filter";

it("Filter component renders correctly", () => {
    const { queryByTestId, queryByPlaceholderText } = render(<Filter/>)
    expect(queryByPlaceholderText("Search")).toBeTruthy()
    expect(queryByTestId("search-button")).toBeTruthy()
    expect(queryByTestId("filter-title-radio")).toBeTruthy()
    expect(queryByTestId("filter-title-label-m")).toBeTruthy()
    expect(queryByTestId("filter-title-label-s")).toBeTruthy()
    expect(queryByTestId("filter-publish-date-radio")).toBeTruthy()
    expect(queryByTestId("filter-publish-date-label-m")).toBeTruthy()
    expect(queryByTestId("filter-publish-date-label-s")).toBeTruthy()    
})