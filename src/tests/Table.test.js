import React from "react";
import { render } from "@testing-library/react";
import Table from "../components/Table";

it("Table component renders correctly", () => {
    const { queryByTestId } = render(<Table lstOfRelatedBooks = {[]}/>)
    expect(queryByTestId("table")).toBeTruthy()
})