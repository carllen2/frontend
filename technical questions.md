Questions:
1. How long did you spend on the coding assignment? 
    a. What would you add to your solution if you had more time?
    b. If you didn't spend much time on the coding test, then use this as an opportunity to explain what you would add.
2. What was the most useful feature that was added to the latest version of your chosen language? Please include a snippet of code that shows how you've used it.
3. How would you track down a performance issue in production? Have you ever had to do this?
4. How would you improve the API that you just used?
5. Please describe yourself using correctly formatted JSON.

Answers:
1. I spent no more than a day working on the assignment.  Because of this, I performed just what the acceptance criteria listed.

If I had more time, I would've performed more test cases, such as testing the functionality of the search function and create a JSON object to test the table component.  In addition to the test cases, I would've created a better layout design for the page.

2. I really liked that EcmaScript brought in new operators so I can code like below.

Ex:
    //before
    let x = undefined;
    if(!x){
    x = 0;
    }

    //now
    let x = undefined;
    x ??= 0;

3. It would depend on the issue at hand and I'm aware that there are 3rd party tools to help, but for most of the time, adding breakpoints/watchlists to the code and making use of the Web Developer tools in the browser is sufficient.  Some of the 3rd party tools that I've used (at least extensively) is Postman.

4. I would improve it by updating or simplifying the documentation, I encountered some issues like the "publish_date" key not existing for some books and the documentation never mentioned that or is hard to find.

5.
{
    first_name: "Carlito",
    last_name: "Llena",
    personality_type: ["humble attitude", "team player", "adaptable", "takes initiative", "open minded", "quick learner", "asks smart questions"],
    BMO_associates: ["my mom"],
    days_available: ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"],
    hours_available: [[12,5],[12,5],[12,5],[12,5],[12,5]],
    time_zone: "EST"
}